<?php
// Require các thư viện PHP
require 'core/connect.php';
require 'core/redirect.php';
require 'core/session.php';
// Kết nối database
$db = new DB();
$db->connect();
$db->set_char('utf8');

$_DOMAIN = 'http://localhost/form/';

$session_selfCreatedAcc = new Session();
$session_selfCreatedAcc->start();
$session_faceBookAcc = new Session();
$session_faceBookAcc->start();

$session_gmailAcc = new Session();
$session_gmailAcc->start();
$session_loginlink = new Session();
$session_loginlink->start();

// Kiểm tra session
if ($session_selfCreatedAcc->get() != ''){
    $user_selfCreatedAcc = $session_selfCreatedAcc->get();
}else{
    $user_selfCreatedAcc = '';
}

// Nếu đăng nhập bằng acc
if ($user_selfCreatedAcc)
{
    // Lấy dữ liệu tài khoản
    $sql_users = "SELECT * FROM usersdata WHERE fullname = '$user_selfCreatedAcc' OR email = '$user_selfCreatedAcc'";
    if ($db->num_rows($sql_users))
    {
        $data_selfCreatedAcc = $db->fetch_assoc($sql_users, 1);
        $shortName = $data_selfCreatedAcc['fullname'];
        if(strlen($shortName) > 15) {
            $shortName = substr($shortName,0,12).'...';
        }
    }
}

// Kiểm tra session_face
if ($session_faceBookAcc->get_face() != ''){
    $user_faceBookAcc = $session_faceBookAcc->get_face();
}else{
    $user_faceBookAcc = '';
}

// Nếu đăng nhập bằng user_face
if ($user_faceBookAcc)
{
    // Lấy dữ liệu tài khoản
    $sql_face = "SELECT * FROM usersdata WHERE first_name = '$user_faceBookAcc'";
    if ($db->num_rows($sql_face))
    {
        $data_faceBookAcc = $db->fetch_assoc($sql_face, 1);
    }
}

// Kiểm tra session gmail
if ($session_gmailAcc->get_gmail() != '')
{
    $user_gmailAcc = $session_gmailAcc->get_gmail();
}
else if ($session_loginlink->get_loginlink() !='')
{
    $user_gmailAcc = $session_gmailAcc->get_loginlink();
}
else
{
    $user_gmailAcc = '';
}
// Nếu đăng nhập bằng gmail
if ($user_gmailAcc)
{
    // Lấy dữ liệu tài khoản
    $sql_gmail = "SELECT * FROM usersdata WHERE email = '$user_gmailAcc'";
    if ($db->num_rows($sql_gmail))
    {
        $data_gmailAcc = $db->fetch_assoc($sql_gmail, 1);
        $shortName = $data_gmailAcc['fullname'];
        if(strlen($shortName) > 15) {
            $shortName = substr($shortName,0,12).'...';
        }
    }
}

?>