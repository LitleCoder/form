<?php
include 'save_session.php';
try{
$conn = new PDO ("mysql:host=localhost;dbname=testlogin;charset=utf8","root","");
$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
if(isset($_POST['reset_password'])){
  $toemail = $_POST['reset_password'];
  $sql = "SELECT * FROM usersdata WHERE email = :email";
  $stmt = $conn->prepare($sql);
  $stmt->bindParam(':email', $toemail, PDO::PARAM_STR);
  $stmt->execute();
  $count = $stmt->rowCount();
  
  if($count == 0){
      echo json_encode(['value'=>$count]);
  }
  
  else{
    $randomString = '';
    //$newpassword = substr(md5( rand(0,999999)), 0,8);
    
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+';
    $chars_lower = 'abcdefghijklmnopqrstuvwxyz';
    $chars_upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $chars_special = '!@#$%^&*()_-=+';
    $chars_number = '0123456789';
    $length = 8;
    // Đảm bảo có ít nhất 1 ký tự thường, 1 ký tự in hoa và 1 ký tự đặc biệt
    $randomString .= $chars_lower[rand(0, strlen($chars_lower) - 1)];
    $randomString .= $chars_upper[rand(0, strlen($chars_upper) - 1)];
    $randomString .= $chars_special[rand(0, strlen($chars_special) - 1)];
    $randomString .= $chars_number[rand(0, strlen($chars_number) - 1)];
    // Tạo phần còn lại của chuỗi
    for ($i = 4; $i < $length; $i++) {
        $randomString .= $chars[rand(0, strlen($chars) - 1)];
    }

    // Trộn các ký tự để tăng độ ngẫu nhiên
    $randomString = str_shuffle($randomString);

 
    $sql = "UPDATE usersdata SET password = ? WHERE email = ?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([md5($randomString),$toemail]);

    require "../library/mailer/PHPMailer-master/src/PHPMailer.php"; 
    require "../library/mailer/PHPMailer-master/src/SMTP.php"; 
    require '../library/mailer/PHPMailer-master/src/Exception.php'; 
    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    try {
        $mail->SMTPDebug = 0; 
        $mail->isSMTP();  
        $mail->CharSet  = "utf-8";
        $mail->Host = 'smtp.gmail.com'; 
        $mail->SMTPAuth = true; 
        $mail->Username = 'viewblog.net@gmail.com'; 
        $mail->Password = 'xdmlyctgiehfsmkj';   
        $mail->SMTPSecure = 'ssl';  
        $mail->Port = 465;                
        $mail->setFrom('viewblog.net@gmail.com', 'viewblog.net' ); 
        $mail->addAddress($toemail);  
        $mail->isHTML(true); 
        $mail->Subject = 'Yêu cầu cấp lại mật khẩu mới';
        $noidungthu = "<p>Chào $toemail.Bạn nhận được thư này là do bạn hoặc ai đó đã yêu cầu cấp lại mật khẩu mới cho tài khoản mà bạn đã đăng ký với https://module-project.great-site.net/ </p><br><h4>Mật khẩu mới của bạn: <span style='color:red'>$randomString</span></h4>"; 
        $mail->Body = $noidungthu;
        $mail->smtpConnect( array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
                "allow_self_signed" => true
            )
            ));
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $flagTime = strtotime(date("H:i:s"));   
        $currentTime = strtotime(date("H:i:s"));  
        $lastclick = 0;
      
        if($resetpass == 1) {
            if($currentTime - $clickTime < 300){
               echo json_encode(['value'=>2]);
            } 
            else if($currentTime - $clickTime >= 300){
              $mail->send();
              $lastclick = 1;
              $session_resetpass->send_pass($lastclick);  
              $session_clickTime->send_clickTime($flagTime);
              echo json_encode(['value'=>$toemail]);
            }    
        }
        
        else{
          $mail->send();
          $lastclick = 1;
          $session_resetpass->send_pass($lastclick);  
          $session_clickTime->send_clickTime($flagTime);
          echo json_encode(['value'=>$toemail]);
        }  
    } 
    catch (Exception $e) {
      echo json_encode(['value'=>3]);
    }
  }
 }
}
catch (PDOException $e) {
  echo json_encode(['value'=>$e->getMessage()]);
}
 $conn = null;
?>
  
