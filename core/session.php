<?php
 
// Lớp session
class Session {
    // Hàm bắt đầu session
    public function start()
    {
        if(!isset($_SESSION)){
            session_start();
        }
    }
 
    // Hàm lưu session acc
    public function send($user_selfCreatedAcc)
    {
        $_SESSION['user_selfCreatedAcc'] = $user_selfCreatedAcc;
    }
 
    // Hàm lấy dữ liệu session acc
    public function get() 
    {
        if (isset($_SESSION['user_selfCreatedAcc']))
        {
            $user_selfCreatedAcc = $_SESSION['user_selfCreatedAcc'];
        }
        else
        {
            $user_selfCreatedAcc = '';
        }
        return $user_selfCreatedAcc;
    }
    // Hàm lưu  user_face
    public function send_face($user_faceBookAcc)
    {
        $_SESSION['user_faceBookAcc'] = $user_faceBookAcc;
    }
 
    // Hàm lấy dữ liệu user_face
    public function get_face() 
    {
        if (isset($_SESSION['user_faceBookAcc']))
        {
            $user_faceBookAcc = $_SESSION['user_faceBookAcc'];
        }
        else
        {
            $user_faceBookAcc = '';
        }
        return $user_faceBookAcc;
    }
      // Hàm lưu session user_gmail
      public function send_gmail($user_gmailAcc)
      {
          $_SESSION['user_gmailAcc'] = $user_gmailAcc;
      }
   
      // Hàm lấy dữ liệu session user_gmail
      public function get_gmail() 
      {
          if (isset($_SESSION['user_gmailAcc']))
          {
              $user_gmailAcc = $_SESSION['user_gmailAcc'];
          }
          else
          {
              $user_gmailAcc = '';
          }
          return $user_gmailAcc;
      }

        // Hàm lưu session reset_pass
        public function send_pass($param)
        {
            $_SESSION['reset_pass'] = $param;
        }
     
        // Hàm lấy dữ liệu session reset_pass
        public function get_pass() 
        {
            if (isset($_SESSION['reset_pass']))
            {
                $reset_pass = $_SESSION['reset_pass'];
            }
            else
            {
                $reset_pass = '';
            }
            return $reset_pass;
        }

        // Hàm lưu session session_clickTime
        public function send_clickTime($parametter)
        {
            $_SESSION['clickTime'] = $parametter;
        }
    
        // Hàm lấy dữ liệu session_clickTime
        public function get_clickTime() 
        {
            if (isset($_SESSION['clickTime']))
            {
                $clickTime = $_SESSION['clickTime'];
            }
            else
            {
                $clickTime = '';
            }
            return $clickTime;
        }

        // Hàm lưu session session_confirm
        public function send_confirm($parametter)
        {
            $_SESSION['confirm'] = $parametter;
        }
    
        // Hàm lấy dữ liệu session_confirm
        public function get_confirm() 
        {
            if (isset($_SESSION['confirm']))
            {
                $confirm = $_SESSION['confirm'];
            }
            else
            {
                $confirm = '';
            }
            return $confirm;
        }

        // Hàm lưu session session_time
        public function send_time($parametter)
        {
            $_SESSION['time'] = $parametter;
        }
    
        // Hàm lấy dữ liệu session_time
        public function get_time() 
        {
            if (isset($_SESSION['time']))
            {
                $time = $_SESSION['time'];
            }
            else
            {
                $time = '';
            }
            return $time;
        }

        // Hàm lưu session_fastlink
        public function send_fastlink($parametter)
        {
            $_SESSION['fastlink'] = $parametter;
        }
    
        // Hàm lấy dữ liệu session_fastlink
        public function get_fastlink() 
        {
            if (isset($_SESSION['fastlink']))
            {
                $fastlink = $_SESSION['fastlink'];
            }
            else
            {
                $fastlink = '';
            }
            return $fastlink;
        }

    // Hàm lưu session_lastclick
    public function send_lastclick($parametter)
    {
        $_SESSION['lastclick'] = $parametter;
    }

    // Hàm lấy dữ liệu session_lastclick
    public function get_lastclick() 
    {
        if (isset($_SESSION['lastclick']))
        {
            $lastclick = $_SESSION['lastclick'];
        }
        else
        {
            $lastclick = '';
        }
        return $lastclick;
    }

    // Hàm lưu session_loginlink
    public function send_loginlink($parametter)
    {
        $_SESSION['loginlink'] = $parametter;
    }

    // Hàm lấy dữ liệu session_loginlink
    public function get_loginlink() 
    {
        if (isset($_SESSION['loginlink']))
        {
            $loginlink = $_SESSION['loginlink'];
        }
        else
        {
            $loginlink = '';
        }
        return $loginlink;
    }

                        
    // Hàm xoá session
    public function destroy() 
    {
        session_destroy();
    }
}
 
?>