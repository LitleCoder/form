<?php
   $email = $_GET['email'];

   $conn = new PDO ("mysql:host=localhost;dbname=testlogin;charset=utf8","root","");
   $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

   $sql_users = "SELECT * FROM  usersdata WHERE email = :email";
   $st_users = $conn->prepare($sql_users);//tao prepare statement
   $st_users->bindParam(':email', $email, PDO::PARAM_STR);
   $st_users->execute();
   $count_users = $st_users->rowCount();

   echo json_encode(['count_users'=>$count_users]);

?>