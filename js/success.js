$_DOMAIN = 'http://localhost/form/';
$('.container__notice').hide(); 
$(document).ready(function(){
//This function	to send data from signup form to server;
$(document).on('click', '.container #btn-feedback', function(e){
    e.preventDefault();
	var	$this = $('#btn-feedback');
		$this.html('');
		$('#btn-feedback').append("<div class='container__loader'></div>");

		$.ajax({
			url : $_DOMAIN + 'core/sendemailagain.php',
			timeout:15000,
			type : 'POST',
			data : {
				email: arrParam[0],
				id: arrParam[1],
				key: arrParam[2]
			}, success : function(data) {
				$saveData = JSON.parse(data); 
				$value = $saveData.value; 
				if($value == 1){
				   $('.container__pe:nth-of-type(2)').show();
				   $('.container__notice').show(); 
				   $('div.container__notice').removeClass('animate');
				   $('div.container__notice').addClass('animate');
			
				   setTimeout(function() {
					   $('div.container__notice').removeClass('animate');
				   }, 700);
			
				   setTimeout(function() {
					$('div.container__notice').hide();
				   }, 5000);
   
				}
				else if($value == 0) {
					$('#show-message').text('Không tìm thấy email');
				}
				else if($value == 2) {
					$('#show-message').text('Rất tiếc! Một liên kết nhanh đã được gửi đi.Vui lòng đợi 5 phút sau bạn mới có thể yêu cầu gửi lại email xác nhận');
					setTimeout(function() {
						$('#show-message').text('');
					   }, 300000)
				}
				else if($value == 3) {
					$('#show-message').text('Lỗi truy vấn sql');
				}
				else if($value == 4) {
					$('#show-message').text('Dữ liệu chưa được khởi tạo');
				}
				else{
					$('#show-message').text($value);
				}
				
			}, error : function(xhr,textStatus,errorThrown) {
				if(textStatus == "timeout"){
					alert('Thời gian phản hồi quá lâu');
				    console.error("Yêu cầu hết thời gian. Chi tiết lỗi:", xhr);
				}
				else if(textStatus === "error"){
					alert('Lỗi kết nối mạng');
				    console.error("Lỗi kết nối mạng");
				}
				else {
					alert('Rất tiếc đã có lỗi xảy ra');
				    console.error("Lỗi không xác định:", errorThrown);
				}

			}
		});

		$(document).ajaxStop(function() {
			$('.container__loader').remove();
			$('#btn-feedback').html('Gửi lại email xác nhận');
			
		});
		
    });
//This function to comeback to signup form
$('#btn-register').on('click',function(){
	window.location.href = $_DOMAIN;
})
});





