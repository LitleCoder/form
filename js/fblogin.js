window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '785517360146755', // FB App ID
      cookie     : true,// enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.2' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
const fblogin_btn = document.getElementById('fblogin');
      fblogin_btn.onclick = fbLogin;

function fbLogin(event) {
    event.preventDefault();
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}


// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        const names = [response.id,response.first_name,response.last_name,response.email,response.link,response.gender,response.picture];
        const obj = {id:names[0],first_name:names[1],last_name:names[2],email:names[3],link:names[4],gender:names[5],picture:names[6]};
        const infoUser = JSON.stringify(obj);

        //localStorage.setItem("names", JSON.stringify(names));

        let fullname = response.first_name+''+response.last_name;
              if(fullname.length > 21) {
                let fullname = fullname.slice(0,20);
              }
        $('.navbar__3rdcon').hide();
        $('header').css('z-index','auto');
        $('#overlay').hide();
        $('#formcontainer').hide();
        $('#navbar__dropdown').show();
        $('#fabLink').html('Logout');
        $('#fabLink').attr("onclick","fbLogout()");
        $('#showName').html('Welcome: '+fullname+'...<span style="font-size:12px;">&#9660;</span>');
        //response.picture.data.url (get picture);
        $('#delete_face').html("Xóa tài khoản");
        $('#delete_face').attr("data-id",response.id);
         //saveUserData(response);
         sendData(infoUser);
    });
}
// function saveUserData(userData){
//     $.post('http://localhost/form/fbData.php', {oauth_provider:'facebook',userData: JSON.stringify(userData)},function(){ return true;});
   
// }
// Logout from facebook
function fbLogout() {
    FB.logout(function() {
        $('#navbar__dropdown').hide();
        $('.navbar__3rdcon').show();
        $('header').css('z-index','-1');
        $('#overlay').show();
        $('#formcontainer').show();    
    });
}
function sendData(infoUser){
$.ajax({
    url : $_DOMAIN + 'core/register.php',
    type : 'POST',
    data : {
        oauth_provider:'facebook',
        userData : infoUser,
      
    }, success : function(data) {
        alert(data);
    }, error : function() {
        alert('Rất tiếc đã xảy ra lỗi!');
    }
});
}