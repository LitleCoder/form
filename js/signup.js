$(document).ready(function(){
$('.formsignup__list').hide();    
$_DOMAIN = 'http://localhost/form/';
$regx = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//function to select html for display birthday
$("#check-date").dateDropdowns({
    submitFieldName: 'check-date',
    dayLabel: 'Ngày',
    monthLabel: 'Tháng',
    yearLabel: 'Năm'
    // defaultDate: '2010-02-17'
});
// Set all hidden fields to type text for the demo
$('input[type="hidden"]').attr('type', 'text').attr('readonly', 'readonly');


//function to show and hide password for signup form
$('#eyeicon').on('click',function(){
    $showPassword = $('#password');
    if($showPassword.prop('type') == 'password'){
        $showPassword.attr('type','text')
    }else{
        $showPassword.attr({"type":"password"});
    }
});

//this function will return to the initial border-color of the select box
$('select').on('change',function(){
    $selectday = $('select.day').val();
    $selectmonth = $('select.month').val();
    $selectyear = $('select.year').val();

    if($selectday != '' && $selectmonth != '' && $selectyear != ''){
        $('.formsignup__Alert').css('display','none');
        $('.formsignup__birthdayAlert').css('display','none');
        $('.formsignup__day').html('Ngày sinh');
        $('use').attr('fill','#586380');
        $('select.day').css('border-color','rgb(219,222,223)');
        $('select.month').css('border-color','rgb(219,222,223)');
        $('select.year').css('border-color','rgb(219,222,223)');
    }
})
//This function to show border-bottom for input field when click;
$('.formsignup__post').on('click',function(){
    $(this).css('border-bottom','2px solid rgb(46,56,86)');
});

$('#password').on('click',function(){
    $('#eyeicon').css('border-bottom','2px solid rgb(46,56,86)');
});


//This function displays border-bottom checks whether the user entered a date of birth or not. ;
$('.formsignup__post').on('keyup',function(){
    $day = $('select.day').val();
    $month = $('select.month').val();
    $year = $('select.year').val();
    if($day == '' || $month == '' || $year == ''){
        $('.formsignup__day').html('');
        $('.formsignup__birthdayAlert').css('display','block');
        $('use').attr('fill','rgb(176,0,32)');
        $('select.day').css('border-color','rgb(176,0,32)');
        $('select.month').css('border-color','rgb(176,0,32)');
        $('select.year').css('border-color','rgb(176,0,32)');
    }
});

//function to hide border-bottom for input field;
var email;
$('#emailSignup.formsignup__post').on('keyup',function(){
   email =  $('#emailSignup').val();
    if(!email){
        $('.formsignup__emailUsedAlert').hide();
        $('#emailSignup.formsignup__post').css('border-bottom','2px solid rgb(46,56,86)');
}
    else{
            $('.formsignup__emailInvalidAlert').css('display','none');
            $('.formsignup__parentEmail').css('display','none');
            $('.formsignup__email').css('display','block');
            $('.formsignup__emailUsedAlert').hide();
    }
})

$('#username.formsignup__post').on('blur',function(){
    $(this).css('border-bottom','none');
});

$('#password.formsignup__post').on('blur',function(){
    $(this).css('border-bottom','none');
    $('#eyeicon').css('border-bottom','none');
});

//this function to check if the email is valid or not
$('#emailSignup.formsignup__post').on('blur',function(){
    $(this).css('border-bottom','none');
    if(email){
        if (!email.match($regx)) {
            $('.formsignup__emailInvalidAlert').css('display','block');
            $('.formsignup__email').css('display','none');
        }
        else{
            $('.formsignup__emailInvalidAlert').css('display','none');
            $url = "http://localhost/form/core/checkEmail.php?email="+email;
            fetch($url)
            .then(data => data.json()) 
            .then(data => {
                if(data.count_users > 0){
                    $('.formsignup__emailUsedAlert').show();
                    $('#emailSignup.formsignup__post').css('border-bottom','2px solid rgb(176,0,32)');
                }
                else{
                    $('.formsignup__emailUsedAlert').hide();
                }
            })
        }
    }
    else{
            $('.formsignup__emailInvalidAlert').css('display','none');
            $('.formsignup__parentEmail').css('display','none');
            $('.formsignup__email').css('display','block');
            $('.formsignup__emailUsedAlert').hide();
    }

});

//function to check username
$('.formsignup__nameAlert').hide();
const validate = document.getElementById('username');
       validate.addEventListener("keyup",validateUserName);
//validate.addEventListener("keyup",checkExist);

function validateUserName(event) {
   let username = document.getElementById('username').value.trim();
   let specialChar = event.code;
   let firstCase = /^[a-zA-Z]/;
   let secondCase = /!|@|#|\$|%|\^|&|\*|\(|\)|=|\+|{|}|\[|\]|;|:|\.|,|\/|`|~/;
   
    if(username){
        if(username.match(firstCase)){
            const message = document.querySelector('.formsignup__nameAlert');
                  message.innerHTML = '';
            try {
                if(username.length < 3){
                    throw 'Tên người dùng của bạn quá ngắn. Độ dài tối thiểu là 3 ký tự.';
                }

               if(username.length > 20){
                    throw 'Tên người dùng của bạn quá dài. Độ dài tối đa là 20 ký tự.';
                }

               if(username.match(secondCase) || specialChar === 'Quote' || specialChar === 'Backslash'){
                    throw 'Tên người dùng của bạn chỉ được chứa các chữ cái, số, gạch dưới và dấu gạch ngang.';
                }

            } 
            catch (error) {
                $('.formsignup__nameAlert').show();
                message.innerHTML = error;
            }
         //check if the username is already in the database;
            const infoError = document.querySelector('.formsignup__nameAlert').innerHTML;
            $url = "http://localhost/form/core/checkName.php?username="+username;

            if(infoError.length == 0) {
            fetch($url)
            .then(d => d.text())
            .then(data => {
                    let recordset = data;  
                if(recordset.length > 0) {
                    let i = 10;
                    let alternateCount = 0;
                    let alternatesFound = new Array(5);
        
                    while (alternateCount < 5 && i < 100) {
                        let potentialName = username + i;
                        if (!recordset.includes(potentialName)) {
                            alternatesFound[alternateCount] = potentialName;
                            alternateCount++;
                        }
                        i++;
                    }
                    $('.formsignup__list').show();
                    $('.formsignup__name').hide();
                    $('.formsignup__nameAlert').show();
                    $('.formsignup__nameAlert').html('Tên người dùng đó đã tồn tại. Hãy thử một trong các đề xuất sau');

                    //select suggesstion name
                    const nameItem = document.querySelectorAll('.formsignup__nameItem');
                          nameItem[0].innerHTML = alternatesFound[0];
                          nameItem[1].innerHTML = alternatesFound[1];
                          nameItem[2].innerHTML = alternatesFound[2];
                          
                    var clickButtons = document.getElementsByClassName("formsignup__nameItem");
                    for (var arr = 0; arr < clickButtons.length; arr++) {
                        clickButtons[arr].addEventListener('click', function(event) {
                            $('.formsignup__nameAlert').hide();
                            $('.formsignup__name').show();
                            var suggesstionName = event.target.innerHTML;
                            document.getElementById('username').value = suggesstionName;
                            
                        });
                    }
                }
        
                else{
                    $('.formsignup__name').show();
                    $('.formsignup__nameAlert').hide();
                }
            })
        }
        }
        else{
            $('.formsignup__nameAlert').css('display','block');
            $('.formsignup__nameAlert').html('Tên người dùng phải bắt đầu bằng chữ cái A-Z và không chứa các ký tự có dấu.');
            return false;
        }
    }

    else{
        $('.formsignup__nameAlert').css('display','none');
        $('.formsignup__nameAlert').hide();
    }
}
      
//function to check password;
$('#password').on("keyup",function(){
    
    $regxPass = /(?=.*\d)(?=.*[a-z])(?=.*\W)(?=.*[A-Z]).{8,}/;
    $pw = $('#password').val().trim();
    if($pw){
        if($pw.length < 8) {
            $(".formsignup__password").css('display','none');
            $(".formsignup__passwordAlert").css('display','block');
            $(".formsignup__passwordAlert").html('Mật khẩu của bạn quá ngắn. Độ dài tối thiểu là 8 ký tự.');
            return false;
        }
        if(!$pw.match($regxPass)) {
            $(".formsignup__password").css('display','none');
            $(".formsignup__passwordAlert").css('display','block');
            $(".formsignup__passwordAlert").html('Vui lòng chọn mật khẩu khó đoán(gồm ký tự thường,ký tự hoa,ký tự số,ký tự không phải là chữ)');
            return false;
        }
        else{
            $(".formsignup__password").css('display','block');
            $(".formsignup__passwordAlert").css('display','none');
        }
    }
    else{
        $(".formsignup__password").css('display','block');
        $(".formsignup__passwordAlert").css('display','none');
    }
});

//function to checkbox
$('#second-checkbox').on("click",function() {
  $checkbox = $('#second-checkbox').is(':checked');
  if($checkbox){
    $('.formsignup__checkboxAlert').css('display','none');
    $('.formsignup__label').css('color','#586380');
    $('.formsignup__highline').css('color','#586380');
  }else{
    $('.formsignup__label').css('color','rgb(176, 0, 32)');
    $('.formsignup__highline').css('color','rgb(176, 0, 32)');
    $('.formsignup__checkboxAlert').css('display','block');
  }
});

//function to show login form
$('#btn-formlogin').on('click',function(){
  $('.formsignup').css('display','none');
    $('.formlogin').css('display','block');
  });

//function to send the data form signup form to server;
$('#formsignup__btn-register').on('click', function(e) {
    e.preventDefault();
    $second_checkbox = $('#second-checkbox').is(':checked');
    $birthday = $('#check-date').val();
    $email = $('#emailSignup').val().trim();
    $username = $('#username').val().trim();
    $password = $('#password').val().trim();

    $email_error = $('.formsignup__emailInvalidAlert');
    $username_error = $('.formsignup__nameAlert');
    $pass_error = $('.formsignup__passwordAlert');

    if($second_checkbox){
        if($birthday){
            if($email_error.css('display') == 'block') {
                $('.formsignup__emailInvalidAlert').show();
            }
            if($username_error.css('display') == 'block') {
                $('.formsignup__nameAlert').show();
            }
            if($pass_error.css('display') == 'block') {
                $('.formsignup__passwordAlert').show();
            }
           
            if(!$email){
                $('#emailSignup.formsignup__post').css('border-bottom','2px solid rgb(176,0,32)');
            }
            if(!$username){
                $('#username.formsignup__post').css('border-bottom','2px solid rgb(176,0,32)');
            }
            if(!$password){
                $('#password.formsignup__post').css('border-bottom','2px solid rgb(176,0,32)');
                $('#eyeicon').css('border-bottom','2px solid rgb(176,0,32)');
            }

            else if($email && $username && $password) {
                $this = $('#formsignup__btn-register');
                $this.html('');
                $('#formsignup__btn-register').append("<div class='formsignup__loader'></div>");

            $.ajax({
                    url : $_DOMAIN + 'core/register.php',
                    timeout:15000,
                    type : 'POST',
                    data : {
                        birthday: $birthday,
                        email : $email,
                        username : $username,
                        password: $password
                    }, success : function(data) {
                        $saveData = JSON.parse(data); 
                        $value = $saveData.value;
                        $name = $saveData.name;
                        //$indexOfId = $value.indexOf('&id');
                        //$partBeforeId = $value.substring(0, $indexOfId);
                        if($value == 1){
                            window.location.href="http://localhost/form/template/success.html?email="+$name;
                        }
                        else if($value == 0) {
                            alert('Rất tiếc mã kích hoạt tài khoản chưa được gửi đến email của bạn');
                        }
                      
                        else{
                            alert($value);
                            console.log($value);
                        }
                    }, error : function(xhr,textStatus,errorThrown) {
                            if(textStatus == "timeout"){
                                alert('Thời gian phản hồi quá lâu');
                            console.error("Yêu cầu hết thời gian. Chi tiết lỗi:", xhr);
                            }
                            else if(textStatus === "error"){
                                alert('Lỗi kết nối mạng');
                            console.error("Lỗi kết nối mạng");
                            }
                            else {
                                alert('Rất tiếc đã có lỗi xảy ra');
                            console.error("Lỗi không xác định:", errorThrown);
                            }
                    }
                });
            }
            $(document).ajaxStop(function() {
                $('.formsignup__loader').remove();
                $('#formsignup__btn-register').html('Đăng ký');
             });
    
        }
        else{
            $('.formsignup__Alert').css('display','block');
        }
       
    }
    else{
        $('.formsignup__label').css('color','rgb(176, 0, 32)');
        $('.formsignup__highline').css('color','rgb(176, 0, 32)');
        $('.formsignup__checkboxAlert').css('display','block');
        $('.formsignup__Alert').css('display','none');
    }
   
}); 

});
