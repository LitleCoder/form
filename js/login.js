$(document).ready(function(){
//function to show and hide password for login form
$('#eyeiconlogin').on('click',function(){
    $showPassword = $('#pwlogin');
    if($showPassword.prop('type') == 'password'){
        $showPassword.attr('type','text')
    }else{
        $showPassword.attr({"type":"password"});
    }
});

})

// Đăng nhập
$('#loginPost').on('click', function(e) {
    e.preventDefault();
    // Gán các giá trị trong các biến
    $user_signin = $('#email').val().trim();
    $pass_signin = $('#pwlogin').val();
 
    if ($user_signin) {
        if($pass_signin){
            $('.formsignin__Alert').hide();
            $this = $('.formlogin__btn');
            $this.html('');
            $('#formlogin .formlogin__btn').append("<div class='formsignup__loader'></div>");
           
            $.ajax({
                url : $_DOMAIN + 'login.php',
                type : 'POST',
                timeout: 15000,
                data : {
                    user_signin : $user_signin,
                    pass_signin : $pass_signin
                }, success : function(data) {
                    $saveData = JSON.parse(data);
                    $getvalue = $saveData.value;
                    if($getvalue == 1) {
                        window.location.href='http://localhost/form/';
                    }
                    else if($getvalue == 0){
                        $('.formsignin__Alert').show();
                        $('.formsignin__Alert').html('Thông tin đăng nhập bạn cung cấp không chính xác. Thử lại...');
                    }
                    else if($getvalue == 2){
                        $('.formsignin__Alert').show();
                        $('.formsignin__Alert').html('Tài khoản của bạn chưa được kích hoạt, vui lòng mở email của bạn để kích hoạt tài khoản.');
                    }
                   
                }, error : function(xhr,textStatus,errorThrown) {
                    if(textStatus == "timeout"){
                        alert('Thời gian phản hồi quá lâu');
                    console.error("Yêu cầu hết thời gian. Chi tiết lỗi:", xhr);
                    }
                    else if(textStatus === "error"){
                        alert('Lỗi kết nối mạng');
                    console.error("Lỗi kết nối mạng");
                    }
                    else {
                        alert('Rất tiếc đã có lỗi xảy ra');
                    console.error("Lỗi không xác định:", errorThrown);
                    }
            }
            });

             $(document).ajaxStop(function() {
                $('.formsignup__loader').remove();
                $this.html('Đăng nhập');
            });

        }
        else{
            $('.formsignin__Alert').show();
            $('.formsignin__Alert').html('Không thể để trống mật khẩu.');
        }
    }
    else{
        $('.formsignin__Alert').show();
        $('.formsignin__Alert').html('Không thể để trống tên người dùng.');
    }
}); 


