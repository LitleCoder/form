$(document).ready(function(){

//function to get email value from signup form when click to fastlink button
    $('#fastlink').on('click',function(){
        $x = $('#emailSignup').val();
        $('.formsignup').hide();
        $('.formfastlink').show();
      function getEmail(email){
        return email;
    }
    document.getElementById('formfastlink-email').value = getEmail($x);
    })

//function to close modal
    $('.formfastlink__close').on('click',function(){
        $('.formfastlink').hide();
        $('.formlogin').show();
    })

    $('.formAlert__close').on('click',function(){
        $('.formAlert').hide();
        $('.formfastlink').show();
    })
//check autofocus
$autofosus = $('#formfastlink-email').attr('autofocus');
if($autofosus) {
    $('.formfastlink__post').css('border-bottom','2px solid rgb(46,56,86)');
}else{
    $('.formfastlink__post').css('border-bottom','none');
}

$('#formfastlink-email').on('keyup',function (){
    $myemail = $('#formfastlink-email').val().trim();
    if(!$myemail){
        $('.formfastlink__email').css('color','rgb(88,99,128)');
        $('.formfastlink__post').css('border-bottom','2px solid rgb(46,56,86)');
        $('.formfastlink__email').html('Email');
    }
    else if($myemail){
        $('.formfastlink__email').css('color','rgb(45,56,86)');
        $('.formfastlink__post').css('border-bottom','2px solid rgb(46,56,86)');
        $('.formfastlink__email').html('Email');
    }
})

//function to submit to server
$('#btn-formfastlink').on('click',function(e){
    e.preventDefault();
    var $this = $('#btn-formfastlink');
      $this.html('');
    $('#btn-formfastlink').append("<div class='formfastlink__loader'></div>");

     $myEmail = $('#formfastlink-email').val().trim();
    if($myEmail){
        const controller = new AbortController();
        const timeoutId = setTimeout(() => controller.abort(), 15000);
    
        $url = "http://localhost/form/core/fastlink.php?email="+$myEmail;
        fetch($url,{ signal: controller.signal })
        .then(data => data.json()) 
        .then(data => {
            clearTimeout(timeoutId);
            $('.formfastlink__loader').remove();
            $this.html('Nhận liên kết nhanh');
            if(data.value == 1){
                $('.formfastlink').hide();
                $('.formAlert').show();
                $('.formAlert__success').text('Chúng tôi đã gửi liên kết đến '+$myEmail);
            }
            else if(data.value == 0){
                $('.formfastlink__email').css('color','rgb(176,0,32)');
                $('.formfastlink__post').css('border-bottom','2px solid rgb(176,0,32)');
                $('.formfastlink__email').text('Không tìm thấy email');
            }
            else if(data.value == 2) {
                $('.formfastlink__email').css('color','rgb(176,0,32)');
                $('.formfastlink__post').css('border-bottom','2px solid rgb(176,0,32)');
                $('.formfastlink__email').text('Rất tiếc! Một liên kết nhanh đã được gửi đi.Vui lòng đợi 5 phút sau bạn mới có thể yêu cầu lại');
                setTimeout(function() {
                    $('.formfastlink__email').text('');
                   }, 300000)
            }
            else{
                $('.formfastlink__email').css('color','rgb(176,0,32)');
                $('.formfastlink__post').css('border-bottom','2px solid rgb(176,0,32)');
                $('.formfastlink__email').text(data.value);
            }
        })
        .catch(error => {
            if (error.name === 'AbortError') {
                alert('Yêu cầu đã bị hủy do thời gian phản hồi quá lâu');
                $('.formfastlink__loader').remove();
                $this.html('Nhận liên kết nhanh');
                console.error('Yêu cầu đã bị hủy do quá thời gian');
            } 
        });
    }
    else{
        $('.formfastlink__loader').remove();
        $this.html('Nhận liên kết nhanh');
        $('.formfastlink__email').css('color','rgb(176,0,32)');
        $('.formfastlink__post').css('border-bottom','2px solid rgb(176,0,32)');
        $('.formfastlink__email').html('Bạn chưa nhập email');
    }
})

}); 
    