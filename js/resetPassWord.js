$_DOMAIN = 'http://localhost/form/';

$('#email').on('keyup',function(){
$email = $('#email').val().trim();
    if(!$email){
        $('.formreset__email').html('Email');
        $('.formreset__post').css('border-bottom','2px solid rgb(46,56,86)');
        $('.formreset__email').css('color','rgb(88,99,128)');
    }
    else{
        $('.formreset__email').html('Email');
        $('.formreset__post').css('border-bottom','2px solid rgb(46,56,86)');
        $('.formreset__email').css('color','rgb(88,99,128)');
    }
})

$('#resetbtn').on('click', function(e) {
    e.preventDefault();
    $myemail = $('#email').val().trim();
            if(!$myemail){
                $('.formreset__email').html('Bạn chưa nhập email');
                $('.formreset__email').css('color','rgb(176,0,32)');
                $('.formreset__post').css('border-bottom','2px solid rgb(176,0,32)');
            }

            else if($myemail) {
                $this = $('#resetbtn');
                $this.html('');
                $('#resetbtn').append("<div class='formreset__loader'></div>");

            $.ajax({
                    url : $_DOMAIN + 'core/resetpassword.php',
                    timeout:15000,
                    type : 'POST',
                    data : {
                        reset_password: $myemail,
                    }, success : function(data) {
                        $saveData = JSON.parse(data); 
                        $value = $saveData.value; 
                        if($value === $myemail){
                            $('.formreset').css('display','none');
                            $('.formAlert').css('display','block');
                            $('.formreset__alert').text('Mật khẩu mới đã được gửi đến '+$myemail);
                        }
                        else if($value == 0){
                            $('.formreset__email').css('color','rgb(176,0,32)');
                            $('.formreset__email').text('Không tìm thấy email');
                        }
                        else if($value == 2){
                            $('.formreset__response').text('Rất tiếc! Một liên kết nhanh đã được gửi.Vui lòng đợi 5 phút sau bạn mới có thể yêu cầu gửi lại password');
                            setTimeout(function() {
                                $('.formreset__response').text('');
                               }, 300000);
                        }
                        else if($value == 3){
                            $('.formreset__email').css('color','rgb(176,0,32)');
                            $('.formreset__email').text('Rất tiếc đã có lỗi vui lòng thử lại');
                        }
                        else{
                            $('.formreset__email').css('color','rgb(176,0,32)');
                            $('.formreset__email').text($value);
                        }
                      
                    }, error : function(xhr,textStatus,errorThrown) {
                            if(textStatus == "timeout"){
                               alert('Thời gian phản hồi quá lâu');
                              console.error("Yêu cầu hết thời gian. Chi tiết lỗi:", xhr);
                            }
                            else if(textStatus === "error"){
                               alert('Lỗi kết nối mạng');
                              console.error("Lỗi kết nối mạng");
                            }
                            else {
                              alert('Rất tiếc đã có lỗi xảy ra');
                              console.error("Lỗi không xác định:", errorThrown);
                            }
                    }
                });
            }

         $(document).ajaxStop(function() {
            $('.formreset__loader').remove();
            $('.formreset__btn').html('Gửi liên kết');;
		 });

   
}); 