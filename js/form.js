$_DOMAIN = 'http://localhost/viewblog/';
$regx = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

// Đăng nhập
$('#formSignin button').on('click', function() {
    $this = $('#formSignin button');
    $this.html('Đang tải ...');
 
    // Gán các giá trị trong các biến
    $user_signin = $('#formSignin #user_signin').val();
    $pass_signin = $('#formSignin #pass_signin').val();
 
    // Nếu các giá trị rỗng
    if ($user_signin == '' || $pass_signin == '')
    {
        $('#formSignin .alert').removeClass('hidden');
        $('#formSignin .alert').html('Vui lòng điền đầy đủ thông tin.');
        $this.html('Đăng nhập');
    }
    //Ngược lại
    else
    {
        $.ajax({
            url : $_DOMAIN + 'function/execute_login.php',
            type : 'POST',
            data : {
                user_signin : $user_signin,
                pass_signin : $pass_signin
            }, success : function(data) {
                $('#formSignin .alert').html(data);
               
            }, error : function() {
                $('#formSignin .alert').removeClass('hidden');
                $('#formSignin .alert').html('Không thể đăng nhập vào lúc này, hãy thử lại sau.');
                $this.html('Đăng nhập');
            }
        });
    }
}); 

//Đăng ký

$('#form-signup #btn_register').on('click', function() {
    $this = $('#form-signup #btn_register');
    $this.html('Đang tải ...');
 
   // Gán các giá trị trong các biến
    $username = $('#form-signup #username').val().trim();
    $email = $('#form-signup #email').val().trim();
    $password = $('#form-signup #password').val();
    $password_confirm = $('#form-signup #password_confirm').val();

   // Nếu các giá trị rỗng
    
    if($username == ""){
        $('#form-signup .alert-uname').removeClass('uname-hidden');
        $('#form-signup .alert-uname').html('Bạn chưa nhập username');
    }
    else if ($username.length < 3)
    {
        $('#form-signup .alert-uname').removeClass('uname-hidden');
        $('#form-signup .alert-uname').html('Tên đăng nhập phải có ít nhất 3 ký tự');
    }

    if ($email == "")
    {
        $('#form-signup .alert-email').removeClass('email-hidden');
        $('#form-signup .alert-email').html('Bạn chưa nhập email');
    }
    else if (!$email.match($regx))
    {
        $('#form-signup .alert-email').removeClass('email-hidden');
        $('#form-signup .alert-email').html('Email không đúng định dạng (ví dụ:name@name.com/.vn/.edu/...)');
    }

    if ($password == "")
    {
        $('#form-signup .alert-password').removeClass('password-hidden');
        $('#form-signup .alert-password').html('Bạn chưa nhập mật khẩu');
    }
    else if ($password.length < 6)
    {
        $('#form-signup .alert-password').removeClass('password-hidden');
        $('#form-signup .alert-password').html('Mật khẩu phải có ít nhất 6 ký tự');
    }

    if ($password_confirm == "")
    {
        $('#form-signup .alert-pass_confirm').removeClass('pass_confirm-hidden');
        $('#form-signup .alert-pass_confirm').html('Bạn chưa nhập lại mật khẩu');
    }
    else if ($password !== $password_confirm)
    {
        $('#form-signup .alert-pass_confirm').removeClass('pass_confirm-hidden');
        $('#form-signup .alert-pass_confirm').html('Mật khẩu nhập lại không khớp');
    }

   
    //Ngược lại
    else
    {
        $("#loading").show();
        $(document).ajaxStop(function() {
            $("#loading").hide();
        });
        
        $.ajax({
            url : $_DOMAIN + 'function/execute_reg.php',
            type : 'POST',
            data : {
                username : $username,
                email : $email,
                password: $password
            }, success : function(data) {
                $('#form-signup .alert').addClass('hidden');
                $('#form-signup .alert').html(data);
                $this.html('Đăng ký tài khoản');
            }, error : function() {
                $('#form-signup .alert').removeClass('hidden');
                $('#form-signup .alert').html('Không thể đăng ký vào lúc này,hãy thử lại sau.');
                $this.html('Đăng ký tài khoản');
                
            }
        });
        
    }
}); 
$('#form-signup #username').on('keyup',function(){
    $email = $('#form-signup #username').val();
    if($email == ""){
        $('#form-signup .alert-uname').addClass('uname-hidden');
    }
});
$('#form-signup #email').on('keyup',function(){
    $email = $('#form-signup #email').val();
    if($email == ""){
        $('#form-signup .alert-email').addClass('email-hidden');
    }
});
$('#form-signup #password').on('keyup',function(){
    $email = $('#form-signup #password').val();
    if($email == ""){
        $('#form-signup .alert-password').addClass('password-hidden');
    }
});
$('#form-signup #password_confirm').on('keyup',function(){
    $email = $('#form-signup #password_confirm').val();
    if($email == ""){
        $('#form-signup .alert-pass_confirm').addClass('pass_confirm-hidden');
    }
});

//lấy lại mật khẩu
$('#formUpdate #forgotPass').on('click', function() {
    $this = $('#formUpdate #forgotPass');
    $this.html('Gửi ...');

    $toemail = $('#formUpdate #update_email').val().trim();

    if($toemail == ""){
        $('#formUpdate .alert-update').removeClass('update-hidden');
        $('#formUpdate .alert-update').html('Bạn chưa nhập email');
    }
    if(!$toemail.match($regx)){
        $('#formUpdate .alert-update').removeClass('update-hidden');
        $('#formUpdate .alert-update').html('Email không đúng định dạng (ví dụ:name@name.com/.vn/.edu/...)');
    }
    else
    {
        $("#resetloading").show();
        $(document).ajaxStop(function() {
            $("#resetloading").hide();
        });
        
        $.ajax({
            url : $_DOMAIN + 'function/resetpass.php',
            type : 'POST',
            data : {
                update_email : $toemail
            }, success : function(data) {
                $('#formUpdate .alert-update').addClass('update-hidden');
                $('#formUpdate .alert-update').html(data);
                $this.html('Gửi');
            }, error : function() {
                $('#formUpdate .alert-update').removeClass('update-hidden');
                $('#formUpdate .alert-update').html('Không thể gửi đi vào lúc này,hãy thử lại sau.');
                $this.html('Gửi');
            }
        });
    }
});
$('#formUpdate #update_email').on('keyup',function(){
    $toemail = $('#formUpdate #update_email').val();
    if($toemail == ""){
        $('#formUpdate .alert-update').addClass('update-hidden');
    }
});
// Xem ảnh trước
function preUpImg() {
    img_up = $('#img_up').val();
    count_img_up = $('#img_up').get(0).files.length;
    $('#formUpImg .box-pre-img').html('<p><strong>Ảnh xem trước</strong></p>');
    $('#formUpImg .box-pre-img').removeClass('hidden');
 
    // Nếu đã chọn ảnh
    if (img_up != '')
    {
        $('#formUpImg .box-pre-img').html('<p><strong>Ảnh xem trước</strong></p>');
        $('#formUpImg .box-pre-img').removeClass('hidden');
        for (i = 0; i <= count_img_up - 1; i++)
        {
            $('#formUpImg .box-pre-img').append('<img src="' + URL.createObjectURL(event.target.files[i]) + '" style="border: 1px solid #ddd; width: 50px; height: 50px; margin-right: 5px; margin-bottom: 5px;"/>');
        }
    } 
    // Ngược lại chưa chọn ảnh
    else
    {
        $('#formUpImg .box-pre-img').html('');
        $('#formUpImg .box-pre-img').addClass('hidden');
    }
}
// Nút reset form  hình ảnh
$('#formUpImg button[type=reset]').on('click', function() {
    $('#formUpImg .box-pre-img').html('');
    $('#formUpImg .box-pre-img').addClass('hidden');
});
// Upload hình ảnh
$('#formUpImg').submit(function(e) {
    img_up = $('#img_up').val();
    count_img_up = $('#img_up').get(0).files.length;
    error_size_img = 0;
    error_type_img = 0;
    $('#formUpImg button[type=submit]').html('Đang tải ...');
 
    // Nếu có chọn ảnh
    if (img_up) {
        e.preventDefault();
         
        // Kiểm tra dung lượng ảnh
        for (i = 0; i <= count_img_up - 1; i++)
        {
            size_img_up = $('#img_up')[0].files[i].size;
            if (size_img_up > 262144) { // 0.25MB 
                error_size_img += 1; // Lỗi
            } else {
                error_size_img += 0; // Không lỗi
            }
        }
 
        // Kiểm tra định dạng ảnh
        for (i = 0; i <= count_img_up - 1; i++)
        {
            type_img_up = $('#img_up')[0].files[i].type;
            if (type_img_up == 'image/jpeg' || type_img_up == 'image/png' || type_img_up == 'image/gif') {
                error_type_img += 0;
            } else {
                error_type_img += 1;
            }
        }
 
        // Nếu lỗi về size ảnh
        if (error_size_img >= 1) {
            $('#formUpImg button[type=submit]').html('Upload');
            $('#formUpImg .alert').removeClass('hidden');
            $('#formUpImg .alert').html('Một trong các tệp đã chọn có dung lượng lớn hơn mức cho phép.');
        // Nếu số lượng ảnh vượt quá 20 file
        } else if (count_img_up > 20) {
            $('#formUpImg button[type=submit]').html('Upload');
            $('#formUpImg .alert').removeClass('hidden');
            $('#formUpImg .alert').html('Số file upload cho mỗi lần vượt quá mức cho phép.');
        } else if (error_type_img >= 1) {
            $('#formUpImg button[type=submit]').html('Upload');
            $('#formUpImg .alert').removeClass('hidden');
            $('#formUpImg .alert').html('Một trong những file ảnh không đúng định dạng cho phép.');
        } else {
            $(this).ajaxSubmit({ 
                beforeSubmit: function() {
                    target:   '#formUpImg .alert', 
                    $("#formUpImg .box-progress-bar").removeClass('hidden');
                    $("#formUpImg .progress-bar").width('0%');
                },
                uploadProgress: function (event, position, total, percentComplete){ 
                    $("#formUpImg .progress-bar").animate({width: percentComplete + '%'});
                    $("#formUpImg .progress-bar").html(percentComplete + '%');
                },
                success: function (data) {     
                    $('#formUpImg button[type=submit]').html('Upload');
                    $('#formUpImg .alert').attr('class', 'alert alert-success'); 
                    $('#formUpImg .alert').html(data);
                },
                error: function() {
                    $('#formUpImg button[type=submit]').html('Upload');
                    $('#formUpImg .alert').removeClass('hidden');  
                    $('#formUpImg .alert').html('Không thể upload hình ảnh vào lúc này, hãy thử lại sau.');
                },
                resetForm: true
            }); 
            return false;
        }
    // Ngược lại không chọn ảnh
    } else {
        $('#formUpImg button[type=submit]').html('Upload');
        $('#formUpImg .alert').removeClass('hidden');
        $('#formUpImg .alert').html('Vui lòng chọn tệp hình ảnh.');
    }
});
// Xoá ảnh chỉ định
$('.del-img').on('click', function() {
    $confirm = confirm('Bạn có chắc chắn muốn xoá ảnh này không?');
    if ($confirm == true)
    {
        $id_img = $(this).attr('data-id');
 
        $.ajax({
            url : $_DOMAIN + 'photos.php',
            type : 'POST',
            data : {
                id_img : $id_img,
                action : 'delete_img'
            },
            success : function() {
                location.reload();
            }
        });
    }
    else
    {
        return false;
    }
});
// Xoá tài khoản chỉ định trong bảng danh sách
$('#delAcc').on('click', function() {
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_acc = $(this).attr('data-id');
 
        $.ajax({
            url : $_DOMAIN + 'function/delete_acc.php',
            type : 'POST',
            data : {
                id_acc : $id_acc,
                action : 'del_acc'
            },
            success : function() {
                location.reload();
            }, error : function() {
                $('#formUpdate .alert-update').html('Không thể xóa vào lúc này,hãy thử lại sau.');
            }
        });
    }
    else
    {
        return false;
    }
});
//xóa tài khoản facebook
$('#delete_face').on('click', function() {
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_face = $(this).attr('data-id');
 
        $.ajax({
            url : $_DOMAIN + 'function/delete_face.php',
            type : 'POST',
            data : {
                id_face : 181220654921262,
                action : 'del_face'
            },
            success : function() {
                location.reload();
            }
        });
    }
    else
    {
        return false;
    }
});
//xóa tài khoản gmail
$('#delete_gmail').on('click', function() {
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_gmail = $(this).attr('data-id');
 
        $.ajax({
            url : $_DOMAIN + 'function/delete_google.php',
            type : 'POST',
            data : {
                id_gmail : $id_gmail,
                action : 'del_gmail'
            },
            success : function() {
                location.reload();
            }
        });
    }
    else
    {
        return false;
    }
});
// Xem ảnh avatar trước
function preUpAvt() {
    img_avt = $('#img_avt').val();
    $('.form-group.box-pre-img').html('<p><strong>Ảnh xem trước</strong></p>');
    $('.form-group.box-pre-img').removeClass('hidden');
  
    // Nếu đã chọn ảnh
    if (img_avt != '')
    {
        $('.form-group.box-pre-img').html('<p><strong>Ảnh xem trước</strong></p>');
        $('.form-group.box-pre-img').removeClass('hidden');
        $('.form-group.box-pre-img').append('<img src="' + URL.createObjectURL(event.target.files[0]) + '" style="border: 1px solid #ddd; width: 50px; height: 50px; margin-right: 5px; margin-bottom: 5px;"/>');
    } 
    // Ngược lại chưa chọn ảnh
    else
    {
        $('.form-group.box-pre-img').html('');
        $('.form-group.box-pre-img').addClass('hidden');
    }
}
// Upload ảnh đại diện
$('#formUpAvt').submit(function(e) {
    img_avt = $('#img_avt').val();
    $('#formUpAvt button[type=submit]').html('Đang tải ...');
  
    // Nếu có chọn ảnh
    if (img_avt) {
        size_img_avt = $('#img_avt')[0].files[0].size;
        type_img_avt = $('#img_avt')[0].files[0].type;
 
        e.preventDefault();
        // Nếu lỗi về size ảnh
        if (size_img_avt > 262144) { // 262144 byte = 0.25MB 
            $('#formUpAvt button[type=submit]').html('Upload');
            $('#formUpAvt .alert-danger').removeClass('hidden');
            $('#formUpAvt .alert-danger').html('Dung lượng ảnh không được quá 0.25MB');
        // Nếu lỗi về định dạng ảnh
        } else if (type_img_avt != 'image/jpeg' && type_img_avt != 'image/png' && type_img_avt != 'image/gif') {
            $('#formUpAvt button[type=submit]').html('Upload');
            $('#formUpAvt .alert-danger').removeClass('hidden');
            $('#formUpAvt .alert-danger').html('File ảnh không đúng định dạng cho phép.');
        } else {
            $(this).ajaxSubmit({ 
                beforeSubmit: function() {
                    target:   '#formUpAvt .alert-danger', 
                    $("#formUpAvt .box-progress-bar").removeClass('hidden');
                    $("#formUpAvt .progress-bar").width('0%');
                },
                uploadProgress: function (event, position, total, percentComplete){ 
                    $("#formUpAvt .progress-bar").animate({width: percentComplete + '%'});
                    $("#formUpAvt .progress-bar").html(percentComplete + '%');
                },
                success: function (data) {     
                    $('#formUpAvt button[type=submit]').html('Upload');
                    $('#formUpAvt .alert-danger').attr('class', 'alert alert-success'); 
                    $('#formUpAvt .alert-success').html(data);
                },
                error: function() {
                    $('#formUpAvt button[type=submit]').html('Upload');
                    $('#formUpAvt .alert-danger').removeClass('hidden');  
                    $('#formUpAvt .alert-danger').html('Không thể upload hình ảnh vào lúc này, hãy thử lại sau.');
                },
                resetForm: true
            }); 
            return false;
        }
    // Ngược lại không chọn ảnh
    } else {
        $('#formUpAvt button[type=submit]').html('Upload');
        $('#formUpAvt .alert-danger').removeClass('hidden');
        $('#formUpAvt .alert-danger').html('Vui lòng chọn tệp hình ảnh.');
    }
});
// Xoá ảnh đại diện
$('#del_avt').on('click', function() {
    $confirm = confirm('Bạn có chắc chắn muốn xoá ảnh đại diện này không?');
    if ($confirm == true)
    {
        $.ajax({
            url : $_DOMAIN + 'functions.php',
            type : 'POST',
            data : {
                action : 'delete_avt'
            }, success : function() {
                location.reload();
            }, error : function() {
                alert('Đã có lỗi xảy ra, vui lòng thử lại.');
            }
        });
    }
    else {
        return false;
    }
});
// Cập nhật thông tin khác
$('#formUpdateInfo button').on('click', function() {
    $('#formUpdateInfo button').html('Đang tải ...');
    $dn_update = $('#dn_update').val();
    $email_update = $('#email_update').val();
 
    if ($dn_update && $email_update) {
        $.ajax({
            url : $_DOMAIN + 'functions.php',
            type : 'POST',
            data : {
                dn_update : $dn_update,
                email_update : $email_update,
                action : 'update_info'
            }, success : function(data) {
                $('#formUpdateInfo .alert').removeClass('hidden');
                $('#formUpdateInfo .alert').html(data);
            }, error : function() {
                $('#formUpdateInfo .alert').removeClass('hidden');
                $('#formUpdateInfo .alert').html('Đã có lỗi xảy ra, vui lòng thử lại.');
            }
        });
        $('#formUpdateInfo button').html('Lưu thay đổi');
    } else {
        $('#formUpdateInfo button').html('Lưu thay đổi');
        $('#formUpdateInfo .alert').removeClass('hidden');
        $('#formUpdateInfo .alert').html('Vui lòng điền đầy đủ thông tin.');
    }
});
$('#formChangePw button').on('click', function() {
    $('#formChangePw button').html('Đang tải ...'); 
    $old_pw_change = $('#old_pw_change').val();
    $new_pw_change = $('#new_pw_change').val();
    $re_new_pw_change = $('#re_new_pw_change').val();
 
    if ($old_pw_change && $new_pw_change && $re_new_pw_change) {
        $.ajax({
            url : $_DOMAIN + 'functions.php',
            type : 'POST',
            data : {
                old_pw_change : $old_pw_change,
                new_pw_change : $new_pw_change,
                re_new_pw_change : $re_new_pw_change,
                action : 'change_pw'
            }, success : function(data) {
                $('#formChangePw .alert').removeClass('hidden');
                $('#formChangePw .alert').html(data);
            }, error : function() {
                $('#formChangePw .alert').removeClass('hidden');
                $('#formChangePw .alert').html('Đã có lỗi xảy ra, vui lòng thủ lại.');
            }
        });
        $('#formChangePw button').html('Lưu thay đổi');
    } else {
        $('#formChangePw button').html('Lưu thay đổi');
        $('#formChangePw .alert').removeClass('hidden');
        $('#formChangePw .alert').html('Vui lòng điền đầy đủ thông tin.');
    }
});



