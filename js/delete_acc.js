$(document).ready(function(){
//xóa tài khoản user acc
$('#delete_acc').on('click', function(event) {
    event.preventDefault();
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_acc = $(this).attr('data-id');
 
        $.ajax({
            url : $_DOMAIN + 'delete_acc.php',
            type : 'POST',
            data : {
                id_acc : $id_acc,
                action : 'del_acc'
            },
            success : function() {
                location.reload();
            }, error : function() {
                alert('Không thể xóa vào lúc này,hãy thử lại sau.');
            }
        });
    }
    else
    {
        return false;
    }
});
//xóa tài khoản facebook
$('#delete_face').on('click', function(event) {
    event.preventDefault();
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_face = $(this).attr('data-id');
  
        $.ajax({
            url : $_DOMAIN + 'delete_acc.php',
            type : 'POST',
            data : {
                id_face :  $id_face,
                action : 'del_face'
            },
            success : function() {
                location.reload();
            }, error : function() {
                alert('Không thể xóa vào lúc này,hãy thử lại sau.');
            }
        });
    }
    else
    {
        return false;
    }
  });
  //xóa tài khoản gmail
  $('#delete_gmail').on('click', function(event) {
    event.preventDefault();
    $confirm = confirm('Bạn có chắc chắn muốn xoá tài khoản này không?');
    if ($confirm == true)
    {
        $id_gmail = $(this).attr('data-id');
  
        $.ajax({
            url : $_DOMAIN + 'delete_acc.php',
            type : 'POST',
            data : {
                id_gmail : $id_gmail,
                action : 'del_gmail'
            },
            success : function() {
                location.reload();
            }, error : function() {
                alert('Không thể xóa vào lúc này,hãy thử lại sau.');
            }
        });
    }
    else
    {
        return false;
    }
  });
  
});
