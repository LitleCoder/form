<?php
// Kết nối database và thông tin chung
require_once 'core/init.php';
 
if (isset($_POST['user_signin']) && isset($_POST['pass_signin']))
{
    // Xử lý các giá trị 
    $user_selfCreatedAcc = trim(htmlspecialchars(addslashes($_POST['user_signin'])));
    $pass_signin = trim(htmlspecialchars(addslashes($_POST['pass_signin'])));
 
        $stmtClassOne = $db->prepare("SELECT fullname FROM usersdata WHERE fullname = ? OR email = ?");
        $stmtClassOne->bind_param("ss",$user_selfCreatedAcc,$user_selfCreatedAcc);
        $stmtClassOne->execute();
        $resultClassOne = $stmtClassOne->get_result();

        if ($resultClassOne->num_rows > 0)
        {
            $pass_signin = md5($pass_signin);
            $stmtClassTwo = $db->prepare("SELECT fullname, password FROM usersdata WHERE (fullname = ? OR email = ?) AND password = ?");
            $stmtClassTwo->bind_param("sss",$user_selfCreatedAcc,$user_selfCreatedAcc,$pass_signin);
            $stmtClassTwo->execute();
            $resultClassTwo = $stmtClassTwo->get_result();
            if ($resultClassTwo->num_rows > 0)
            {
                $stmtClassThird = $db->prepare("SELECT fullname, password, active FROM usersdata WHERE (fullname = ? OR email = ?) AND password = ? AND active = '1'");
                $stmtClassThird->bind_param("sss",$user_selfCreatedAcc,$user_selfCreatedAcc,$pass_signin);
                $stmtClassThird->execute();
                $resultClassThird = $stmtClassThird->get_result();
                // Nếu username và password khớp và tài khoản không bị khoá (active = 1)
                if ($resultClassThird->num_rows > 0)
                {
                    // Lưu session
                    $session_selfCreatedAcc->send($user_selfCreatedAcc);
                    echo json_encode(['value'=>1]);
                }
                else
                {
                    echo json_encode(['value'=>2]);
                }
            }
            else
            {
                echo json_encode(['value'=>0]);
            }
        }
        // Ngược lại không tồn tại username
        else
        {
            echo json_encode(['value'=>0]);
        }
    }
// Ngược lại không tồn tại phương thức post
else
{
    new Redirect($_DOMAIN); 
}
 
?>