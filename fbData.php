<?php
require 'core/init.php';

$facebook = $_POST['userData'];
$userData = json_decode($facebook);
// $userData = json_decode($_POST['userData']);
$save_name = $userData->first_name;
if (!empty($userData)) {
    $oauth_provider = $_POST['oauth_provider']; 
    $link = !empty($userData->link)?$userData->link:''; 
    $gender = !empty($userData->gender)?$userData->gender:''; 

    // Check if user exists
    $prevQuery = "SELECT id FROM usersdata WHERE oauth_id = ?";
    $stmt = $db->cn->prepare($prevQuery);
    $stmt->bind_param("s", $userData->id);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        // Update existing user's information
        $query = "UPDATE usersdata SET first_name = ?, last_name = ?, email = ?, gender = ?, picture = ?, url = ?, active = 1, modified = NOW() WHERE oauth_provider = ? AND oauth_id = ?";
        $stmt = $db->cn->prepare($query);
        $stmt->bind_param("ssssssss", $userData->first_name, $userData->last_name, $userData->email, $gender, $userData->picture, $link, $oauth_provider, $userData->id);
        $stmt->execute();
        $session_faceBookAcc->send_face($save_name);
        new Redirect($_DOMAIN);
    } 
    else {
        // Insert new user only if they don't exist
        $query = "INSERT INTO usersdata (oauth_provider,oauth_id,first_name,last_name,email,gender,picture,url,active,created_at,modified) VALUES (?,?,?,?,?,?,?,?,1,NOW(),NOW())";
        $stmt = $db->cn->prepare($query);
        $stmt->bind_param("ssssssss", $oauth_provider, $userData->id, $userData->first_name, $userData->last_name, $userData->email, $gender, $userData->picture, $link);
        $stmt->execute();
        $session_faceBookAcc->send_face($save_name);
        new Redirect($_DOMAIN);

    }

    $stmt->close();
}
?>
