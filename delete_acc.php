<?php
 
// Kết nối database và thông tin chung
require 'core/init.php';
// delete user acc;
if ($user_selfCreatedAcc) {
    if (isset($_POST['id_acc'])) {
        $action = trim(addslashes(htmlspecialchars($_POST['action'])));
        if ($action == 'del_acc') {       
            $id = trim(htmlspecialchars(addslashes($_POST['id_acc'])));
            $sql_check_id = "SELECT id FROM usersdata WHERE id = '$id'";

            if ($db->num_rows($sql_check_id)) {
                $sql_del_acc = "DELETE FROM usersdata WHERE id = '$id'";
                $db->query($sql_del_acc);
                $session_selfCreatedAcc->destroy();
                header('http://localhost/form/');
            } 
            else {
                echo 'Rất tiếc Id của bạn đã được thay đổi';
            }      
        } 
        else {
            echo 'Bạn đang cố thực hiện hành vi không được phép';
        }
    }

    else {
        echo 'Id bạn cung cấp không đúng';
    }
}
else {
    echo 'Bạn phải đăng nhập trước';
}


// delete google acc;
if ($user_gmailAcc) {

    if (isset($_POST['id_gmail'])) {
        $action = trim(addslashes(htmlspecialchars($_POST['action'])));
 
        if ($action == 'del_gmail') {       
            $id = trim(htmlspecialchars(addslashes($_POST['id_gmail'])));
            $sql_check_id_exist = "SELECT id FROM usersdata WHERE id = '$id'";

            if ($db->num_rows($sql_check_id_exist)) {
                $sql_del_gmail = "DELETE FROM usersdata WHERE id = '$id'";
                $db->query($sql_del_gmail);
                $session_gmailAcc->destroy();
                header('http://localhost/form/');
            }   
            else {
                echo 'Rất tiếc Id của bạn đã được thay đổi';
            }       
        }
        else {
            echo 'Bạn đang cố thực hiện hành vi không được phép';
        }
    }
    else {
        echo 'Id bạn cung cấp không đúng';
    }
}

else {
    echo 'Bạn phải đăng nhập trước';
}


 // delete facebook acc;
 if ($user_faceBookAcc) 
 {
     if (isset($_POST['id_face']))
     {
         $action = trim(addslashes(htmlspecialchars($_POST['action'])));
  
  if ($action == 'del_face')
 {       
     $id = trim(htmlspecialchars(addslashes($_POST['id_face'])));
     $sql_check = "SELECT id FROM usersdata WHERE id = '$id'";
     if ($db->num_rows($sql_check))
     {
         $sql_del_face = "DELETE FROM usersdata WHERE id = '$id'";
         $db->query($sql_del_face);
         $session_faceBookAcc->destroy();
         header('http://localhost/form/');
     }       
 }   
     }
     else
     {
         header('http://localhost/form/'); 
     }
 }