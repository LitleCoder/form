<?php
    require 'core/init.php';
    require 'core/login_gmail.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quizlet</title>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/formsignup.css">
    <link rel="stylesheet" href="css/formlogin.css">
    <link rel="stylesheet" href="css/formfastlink.css">
    <script src='js/jquery.min.js'></script>
</head>
<body>
   
     <header> 
           <div class="navbar">
                <div class="navbar__1stcon">
                    <div class='navbar__listItem '><img src="image/quizlet.svg" alt="quizlet-logo"></div>
                    <a href='http://localhost/form/template/voicafe.html' class='navbar__listItem'>Đề phỏng vấn 1</a>
                    <div class='navbar__listItem'>Chủ đề &nbsp; <span><img src="image/dropdown-bold.svg" alt="dropdown-svg" width='18' height='18'></span></div>
                </div>

                <div class="navbar__2ndcon">
                    <img class="navbar__searchIcon" src='image/search.svg' alt='search-icon' width='18' height='18'>
                    <input type='text' class="navbar__searchText" placeholder='Tìm kiếm bất cứ điều gì'>
                </div>

                <div class="navbar__3rdcon">
                    <div class="navbar__image">
                        <img class="navbar__imageItem" src="image/sum.svg" alt="sum-icon" width='22' height='22'>
                    </div>
                    <button type='button' class="navbar__signin" id='signin'>Đăng nhập</button>
                    <button type='button' class="navbar__signup" id='signup'>Đăng ký</button>
                </div>

                <?php
                    if($user_gmailAcc) {
                        echo'<script>
                                $(".navbar__3rdcon").hide();
                                $("header").css("z-index","auto");
                            </script>

                            <div class="navbar__dropdown" onclick="showMenu()">
                                <button type="button" class="navbar__dropbtn" >Welcome: '.$shortName.'<span style="font-size:12px;">&#9660;</span></button>
                                <div id="myDropdown" class="navbar__dropdown-content">
                                    <a href="'.$_DOMAIN.'signout.php">Logout</a>
                                    <a data-id="'.$data_gmailAcc['id'].'" id="delete_gmail">Xóa tài khoản</a>
                                </div>
                            </div>
                        ';
                    }
                    if($user_selfCreatedAcc) {
                        echo'<script>
                                $(".navbar__3rdcon").hide();
                                $("header").css("z-index","auto");
                            </script>

                            <div class="navbar__dropdown" onclick="showMenu()">
                                <button class="navbar__dropbtn" >Welcome: '.$shortName.' <span style="font-size:12px;">&#9660;</span></button>
                                <div id="myDropdown" class="navbar__dropdown-content">
                                    <a href="'.$_DOMAIN.'signout.php">Logout</a>
                                    <a data-id="'.$data_selfCreatedAcc['id'].'" id="delete_acc">Xóa tài khoản</a>
                                </div>
                            </div>
                        ';
                    }
                ?> 
                <div class="navbar__dropdown" id="navbar__dropdown" onclick="showMenu()">
                    <button type="button" class="navbar__dropbtn" id="showName"></button>
                    <div id="myDropdown" class="navbar__dropdown-content">
                        <a href="#" onclick="fbLogout()">Logout</a>
                        <a href='#' onclick="alert('Ở chế độ phát triển không có dữ liệu để xóa.')">Xóa tài khoản</a>
                    </div>
                </div>
           </div>
    </header> 
    <div id="overlay"></div>
    <section id='formcontainer'>
        <!-- UI signup form -->
        <div class="formsignup">
            <h1 class="formsignup__title">Đăng ký miễn phí để tạo học phần</h1>
            <a href="<?= $client->createAuthUrl();?>" style="text-decoration:none">
                <div class="formsignup__btn">
                    <img src="image/google-icon.svg" alt="google-icon" with='24' height='24'> &nbsp;&nbsp;
                    <p>Tiếp tục với Google</p>
                </div>
            </a>
            <div class="formsignup__btn" id='fblogin'>
                <img src="image/facebook-icon.svg" alt="face-icon" with='24' height='24'> &nbsp;&nbsp;
                <p>Tiếp tục với facebook</p>
            </div>

            <div class="formsignup__delta">
                <div class="formsignup__1stdelta"></div>
                <p>hoặc email</p>
                <div class="formsignup__2nddelta"></div>
            </div>

            <form method='post' id='formsignup'>
                <div class="formsignup__birthday">
                    <p class="formsignup__birthdayAlert">Vui lòng nhập ngày sinh</p>
                    <p class="formsignup__text formsignup__day">Ngày sinh</p>&nbsp;&nbsp;
                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" aria-label="Thông tin" class="AssemblyIcon AssemblyIcon--medium" role="img" width="24"  height="24" >
                        <defs>
                            <symbol id="circle-info" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C6.48 2 2 6.48 2 12C2 17.52 6.48 22 12 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 12 2ZM12 7C11.4477 7 11 7.44772 11 8C11 8.55228 11.4477 9 12 9C12.5523 9 13 8.55228 13 8C13 7.44772 12.5523 7 12 7ZM11 16C11 16.55 11.45 17 12 17C12.55 17 13 16.55 13 16V12C13 11.45 12.55 11 12 11C11.45 11 11 11.45 11 12V16ZM4 12C4 16.41 7.59 20 12 20C16.41 20 20 16.41 20 12C20 7.59 16.41 4 12 4C7.59 4 4 7.59 4 12Z"></path>
                            </symbol>
                        </defs>
                        <noscript fill="#586380"></noscript>
                        <use xlink:href="#circle-info" fill="#586380"></use>
                        <noscript fill="#586380"></noscript>
                    </svg>
                </div>

                <input type="hidden" id='check-date'style="display:none;">

                <p class='formsignup__text formsignup__emailInvalidAlert'>Địa chỉ email không hợp lệ</p>

                <div class="formsignup__emailUsedAlert">
                    <p style='color:rgb(176,0,32)'>Có vẻ như địa chỉ email này đã được đăng ký. Thay vào đó:</p>

                    <div  class="formsignup__optionAlert">
                        <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" aria-label="dấu mũ chỉ xuống" class="AssemblyIcon AssemblyIcon--small" role="img" width="24"  height="24" >
                        <defs>
                            <symbol id="caret-down" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M7.14541 8.35535L12.0063 13.0691L16.8671 8.35535C17.3557 7.88155 18.145 7.88155 18.6336 8.35535C19.1221 8.82916 19.1221 9.59453 18.6336 10.0683L12.8832 15.6446C12.3946 16.1185 11.6054 16.1185 11.1168 15.6446L5.36644 10.0683C4.87785 9.59453 4.87785 8.82916 5.36644 8.35535C5.85503 7.8937 6.65682 7.88155 7.14541 8.35535Z"></path>
                            </symbol>
                            <text></text>
                        </defs>
                        <noscript fill="#586380"></noscript>
                        <use xlink:href="#caret-down" fill="#5255FF"></use>
                        <noscript fill="#586380"></noscript>
                        </svg> 
                        <p style='color:#5255FF'> &nbsp;Hãy đăng nhập</p>
                    </div>

                    <div  class="formsignup__optionAlert">
                        <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" aria-label="dấu mũ chỉ xuống" class="AssemblyIcon AssemblyIcon--small" role="img" width="24"  height="24" >
                        <defs>
                            <symbol id="caret-down" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M7.14541 8.35535L12.0063 13.0691L16.8671 8.35535C17.3557 7.88155 18.145 7.88155 18.6336 8.35535C19.1221 8.82916 19.1221 9.59453 18.6336 10.0683L12.8832 15.6446C12.3946 16.1185 11.6054 16.1185 11.1168 15.6446L5.36644 10.0683C4.87785 9.59453 4.87785 8.82916 5.36644 8.35535C5.85503 7.8937 6.65682 7.88155 7.14541 8.35535Z"></path>
                            </symbol>
                            <text></text>
                        </defs>
                        <noscript fill="#586380"></noscript>
                        <use xlink:href="#caret-down" fill="#5255FF"></use>
                        <noscript fill="#586380"></noscript>
                        </svg> 
                        <p style='color:#5255FF' id='fastlink'> &nbsp;Yêu cầu một liên kết nhanh</p>
                    </div>
                </div>

                <p class='formsignup__text formsignup__email'>Email</p>
                <p class='formsignup__text formsignup__parentEmail'>Email của bố mẹ</p>
                <input type="email" id='emailSignup' class='formsignup__post' placeholder='tên@email.com'>
                <div class='formsignup__text formsignup__nameAlert'></div>
                <p class='formsignup__text formsignup__name'>Tên người dùng</p>
                <input type="text" id='username' class='formsignup__post' placeholder='andrew123'>
                
                <div class='formsignup__list'>
                    <p class='formsignup__nameItem'></p>
                    <p class='formsignup__nameItem'></p>
                    <p class='formsignup__nameItem'></p>
                </div>

                <p class='formsignup__text formsignup__password'>Mật khẩu</p>
                <p class='formsignup__text formsignup__passwordAlert'></p>

                <div class="formsignup__pass">
                    <input id='password' type="password" class='formsignup__post formsignup__placeholder' placeholder='&nbsp;&nbsp;&nbsp;&nbsp;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;'>
                    <img id='eyeiconsignup' src="image/eyes.svg" alt="eye-icon" width='100%'>
                </div>

                <input type='checkbox' id='first-checkbox'>
                <label class='formsignup__label' for="first-checkbox">&nbsp;&nbsp; Tôi là giáo viên</label>
                <br>
                
                <input type="checkbox" id='second-checkbox'>
                <label class='formsignup__label' for="second-checkbox">&nbsp;&nbsp; Tôi chấp nhận <b class='formsignup__highline'>Điều khoản dịch vụ</b> và <b class='formsignup__highline'>Chính sách quyền riêng tư</b> của vQuizlet </label>
                <div class="formsignup__checkboxAlert">
                    <ul>
                        <li  class="formsignup__checkboxAlertList">VUI LÒNG CHẤP NHẬN ĐIỀU KHOẢN DỊCH VỤ VÀ CHÍNH SÁCH QUYỀN RIÊNG TƯ CỦA vQUIZLET ĐỂ TIẾP TỤC. </li>
                        <li class="formsignup__checkboxAlertList">Thật đáng tiếc, đã xảy ra lỗi 😅. Xin vui lòng thử lại.</li>
                    </ul>
                </div>
                <div class="formsignup__Alert">Thật đáng tiếc, đã xảy ra lỗi 😅. Xin vui lòng thử lại.</div>  
                <button type="button" class='formsignup__btn formsignup__btn-background' id='formsignup__btn-register'>Đăng ký  </button>
                
            </form>
            <button type="button" class='formsignup__btn formsignup__btn-replace' id='btn-formlogin'>Bạn đã có tài khoản rồi?Đăng nhập</button>
        </div> 

        <!-- UI login form -->
        <div class="formlogin">
            <h1 class="formlogin__title">Đăng nhập</h1>
            <div class="formlogin__btn formlogin__btn-replace">
                <img src="image/google-icon.svg" alt="google-icon" with='24' height='24'> &nbsp;&nbsp;
                <p>Đăng nhập bằng Google</p>
            </div>

            <div class="formlogin__btn formlogin__btn-replace">
                <img src="image/facebook-icon.svg" alt="face-icon" with='24' height='24'> &nbsp;&nbsp;
                <p>Đăng nhập bằng facebook</p>
            </div>

            <div class="formlogin__btn formlogin__btn-replace" onclick='alert("Chức năng phải trả phí nên tui chưa làm")'>
                <img src="image/apple-icon.svg" alt="apple-icon" with='24' height='24'> &nbsp;&nbsp;
                <p>Đăng nhập bằng apple</p>
            </div>

            <div class="formlogin__delta">
                <div class="formlogin__1stdelta"></div>
                <p>hoặc email</p>
                <div class="formlogin__2nddelta"></div>
            </div>

            <form method='post' id='formlogin'>
                <p class='formlogin__text formlogin__emailInvalidAlert'>Địa chỉ email không hợp lệ</p>

                <p class='formlogin__text formlogin__email'>Email</p>
                <input type="text" id='email' class='formlogin__post' placeholder='Nhập địa chỉ chỉ email hoặc tên người dùng của bạn'>

                <p class='formlogin__text formlogin__password'>Mật khẩu</p>
                <p class='formlogin__text formlogin__passwordAlert'></p>
                <a href='http://localhost/form/template/reset.html' target='_blank' class='formlogin__text formlogin__resetpassword'>Quên mật khẩu</a>
                <div class="formlogin__pass">
                    <input id='pwlogin' type="password" class='formlogin__post formlogin__placeholder' placeholder='Nhập mật khẩu của bạn'>
                    <img id='eyeiconlogin' src="image/eyes.svg" alt="eye-icon" width='100%'>
                </div>
                <p class='formlogin__text'>Bằng cách nhấp Đăng nhập, bạn chấp nhận <b class='formlogin__highline'>Điều khoản dịch vụ</b> Và <b class='formlogin__highline'>Chính sách quyền riêng tư</b> của Quizlet</p>
                <div class="formsignin__Alert"></div> 
                <button type="button"id='loginPost' class='formlogin__btn formlogin__btn-background'>Đăng nhập</button>
                <a class='formlogin__fastlink' href='#' id='showform-fastlink'>Đăng nhập bằng liên kết nhanh</a>
            </form>
        </div> 

        <!-- UI fast link form -->
        <div class="formfastlink">
            <p class="formfastlink__close">X</p>
            <h1 class="formfastlink__title">Yêu cầu một liên kết nhanh</h1>
            <div>
                <p>Hãy nhập email bạn đã dùng để đăng ký và chúng tôi sẽ gửi cho bạn liên kết để đăng nhập.</p>
            </div>
            <form method='post' id='formfastlink'>
                    <p class='formfastlink__text formfastlink__email'>Email</p>
                    <input type="email" id='formfastlink-email' class='formfastlink__post formfastlink__placeholder' placeholder='name@email.com' autofocus>
                    <div class="formfastlink__delta"></div>
                    <div>                    
                        <button type='button' id='btn-formfastlink' class='formfastlink__btn formfastlink__btn-background'>Nhận liên kết nhanh</button>
                    </div>
            </form>
        </div>
        
        <!-- UI sent successfully fast link -->
        <div class="formAlert">
            <p class="formAlert__close">X</p>
            <h1 class="formAlert__title">Yêu cầu gửi một liên kết nhanh</h1>
            <div>
                <p class="formAlert__success"></p>
                <p>Vui lòng kiểm tra hộp thư rác nếu bạn không thấy email trong hộp thư đến của mình</p>
                <a href="#">Bạn cần thêm hỗ trợ?</a> 
            </div>
        </div>


    </section>
    <script src='js/jquery.date-dropdowns.js'></script>
    <script src='js/signup.js'></script>
    <script src='js/login.js'></script>
    <script src='js/header.js'></script>
    <script src='js/fblogin.js'></script>
    <script src='js/formfastlink.js'></script>
    <script src='js/delete_acc.js'></script>
    <?php
         if($user_selfCreatedAcc || $user_gmailAcc){
            echo '<script>
                  $("#overlay").hide();
                  $("#formcontainer").hide();
                 </script>';
         }
    ?>    
</body>
</html>
