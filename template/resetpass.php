<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/formreset.css">
    <script src='../js/jquery.min.js'></script>
    <title>Reset</title>
</head>
<body>
<div class="formreset">
            <h1 class="formreset__title">Cài đặt lại mật khẩu</h1>

            <form method='post' id='formreset'>
                <p class='formreset__text'>
                    Hãy nhập email bạn đã dùng để đăng ký. 
                    Chúng tôi sẽ gửi cho bạn một liên kết để đăng nhập và đặt lại mật khẩu của bạn. 
                    Nếu bạn đăng ký bằng email của phụ huynh, chúng tôi sẽ gửi liên kết cho họ.
                </p>

                <p class='formreset__text formreset__email'>Email</p>
                <input type="email" id='email' class='formreset__post' placeholder='name@email.com'>
                <p class='formreset__response'></p>
                <button type="button" id='resetbtn' class='formreset__btn formreset__btn-background'style='width:20%'>Gửi liên kết</button>
            </form>
</div> 

<div class="formAlert">
    <h1>Kiểm tra email của bạn</h1>
    <p class='formreset__text formreset__alert'></p>
    <p class='formreset__text'>Vui lòng kiểm tra hộp thư rác nếu bạn không nhìn thấy email trong hộp thư của mình?</p>
    <br>
    <a class='formreset__support' href='#' onclick='alert("Chức năng đang phát triển")'>Bạn cần thêm hỗ trợ?</a>
</div>

 <script src='../js/resetPassWord.js'></script>   
</body>
</html>